package com.gzccc.fare.admin.dao;

import com.gzccc.fare.core.dto.NoticeCondition;
import com.gzccc.fare.core.dto.TicketAddCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import com.gzccc.fare.core.model.Line;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/29 11:56
 * @Version 1.0
 * @Content
 */
@Mapper
public interface TicketDao {
    List<TicketCondition> list();

    void add(TicketAddCondition ticketAddCondition);

    TicketCondition getId(Integer id);

    void edit(TicketCondition ticketCondition);

    List<TicketCondition> query(Line line);

    void delete(Integer id);

    void notice(NoticeCondition noticeCondition);

    List<NoticeCondition> noticeList();
}
