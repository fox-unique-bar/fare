package com.gzccc.fare.admin.controller;

import com.gzccc.fare.admin.service.TicketService;
import com.gzccc.fare.core.dto.NoticeCondition;
import com.gzccc.fare.core.dto.TicketAddCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import com.gzccc.fare.core.model.Line;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.List;


/**
 * @Author: 杨友奇
 * @Date: 2018/10/28 23:56
 * @Version 1.0
 * @Content 对车讯进行操作
 */
@Controller
@RequestMapping("ticket")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    /**
     * 进入车讯管理主页（车讯列表）
     * @param model
     * @return
     */
    @GetMapping("/index")
    public String index(Model model){
        List<TicketCondition> ticketConditions = ticketService.list();
        model.addAttribute("ticket",ticketConditions);
        return "/ticket/index";
    }

    /**
     * 进入添加车讯页面
     * @return
     */
    @GetMapping("/add")
    public String add(){
        return "/ticket/add";
    }

    /**
     * 进行车讯添加操作
     * @param ticketAddCondition
     * @param bindingResult
     * @return
     */
    @PostMapping("/add")
    public String add(@Valid TicketAddCondition ticketAddCondition, BindingResult bindingResult){
        ticketService.add(ticketAddCondition);
        return "redirect:/ticket/index";
    }


    /**
     * 进入车讯查询页面
     * @param model
     * @return
     */
    @GetMapping("/query")
    public String query(Model model){
        List<TicketCondition> ticketConditions = ticketService.list();
        model.addAttribute("ticket",ticketConditions);
        return "/ticket/query";
    }

    /**
     * 通过起始站终点站进行判断查询
     * @param line ：线路表，包含起点站终点站
     * @param model
     * @return
     */
    @PostMapping("/query")
    public String query(@Valid Line line, Model model){
        List<TicketCondition> ticketConditions = ticketService.query(line);
        model.addAttribute("ticket",ticketConditions);
        return "/ticket/query";
    }

    /**
     * 进入车讯修改页面
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/edit")
    public String edit(@RequestParam Integer id, Model model){
        TicketCondition ticketCondition = ticketService.getId(id);
        model.addAttribute("ticket",ticketCondition);
        return "/ticket/edit";
    }

    /**
     * 只能对日期，票价进行更改操作，并重定向到主页
     * @param ticketCondition
     * @param bindingResult
     * @return
     */
    @PostMapping("/edit")
    public String edit(@Valid TicketCondition ticketCondition, BindingResult bindingResult){
         ticketService.edit(ticketCondition);
        return "redirect:/ticket/index";
    }

    /**
     * 通过ID进行删除车讯并重定向返回到主页
     * @param id
     * @return
     */
    @GetMapping("/delete")
    public String delete(@RequestParam Integer id){
        ticketService.delete(id);
        return "redirect:/ticket/index";
    }

    /**
     * 进入公告页面
     * @param model
     * @return
     */
    @GetMapping("/notice")
    public String notice(Model model){
       List<NoticeCondition> noticeConditions = ticketService.noticeList();
        model.addAttribute("notice",noticeConditions);
        return "/ticket/notice";
    }

    /**
     * 编辑公告并重定向到公告页面
     * @param noticeCondition
     * @param bindingResult
     * @return
     */
    @PostMapping("/notice")
    public String notice(@Valid NoticeCondition noticeCondition,BindingResult bindingResult){
        ticketService.notice(noticeCondition);
        return "redirect:/ticket/notice";
    }

}
