package com.gzccc.fare.admin.controller;

import com.gzccc.fare.admin.service.CustomerService;
import com.gzccc.fare.core.dto.AdminOrderCondition;
import com.gzccc.fare.core.dto.CustomerCondition;
import com.gzccc.fare.core.dto.OrderCondition;
import com.gzccc.fare.core.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/29 1:05
 * @Version 1.0
 * @Content 对客户进行操作
 */
@Controller
@RequestMapping("customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    /**
     * 进入客户管理主页（及订单查询页面）
     * @return
     */
    @GetMapping("/index")
    public String index(Model model){
        List<OrderCondition> orderConditionList = customerService.list();
        model.addAttribute("orders",orderConditionList);
        return "/customer/index";
    }

    /**
     * 通过用户名进行模糊查询，查询结果便放回订单查询页面
     * @param adminOrderCondition
     * @param bindingResult
     * @param model
     * @return
     */
    @PostMapping("/index")
    public String index(@Valid AdminOrderCondition adminOrderCondition, BindingResult bindingResult, Model model){
        List<AdminOrderCondition> list =customerService.indexQuery(adminOrderCondition);
        model.addAttribute("orders",list);
        return "/customer/index";
    }

    /**
     * 进入用户查询页面
     * @return
     */
    @GetMapping("/query")
    public String query(Model model){
        List<CustomerCondition> customerConditionList = customerService.customerList();
        model.addAttribute("customer",customerConditionList);
        return "/customer/query";
    }

    /**
     * 通过用户名查询用户信息，并返回到用户查询页面
     * @param customerCondition
     * @param bindingResult
     * @param model
     * @return
     */
    @PostMapping("/query")
    public String query(@Valid CustomerCondition customerCondition, BindingResult bindingResult,Model model){
        List<CustomerCondition> customerConditionList =customerService.query(customerCondition);
        model.addAttribute("customer",customerConditionList);
        return "/customer/query";
    }

    /**
     * 通过ID修改用户状态为正常并返回到用户查询页面
     * @param id
     * @return
     */
    @GetMapping("/normal")
    public String normal(@RequestParam Integer id){
        customerService.normal(id);
        return "redirect:/customer/query";
    }
    /**
     * 通过ID修改用户状态为禁用并返回到用户查询页面
     * @param id
     * @return
     */
    @GetMapping("/forbidden")
    public String forbidden(@RequestParam Integer id){
        customerService.forbidden(id);
        return "redirect:/customer/query";
    }


}
