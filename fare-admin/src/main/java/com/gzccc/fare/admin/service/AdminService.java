package com.gzccc.fare.admin.service;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/28 23:05
 * @Version 1.0
 * @Content
 */
public interface AdminService {
    public String login(String username, String password);
}
