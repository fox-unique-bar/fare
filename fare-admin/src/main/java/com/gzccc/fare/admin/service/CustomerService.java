package com.gzccc.fare.admin.service;

import com.gzccc.fare.core.dto.AdminOrderCondition;
import com.gzccc.fare.core.dto.CustomerCondition;
import com.gzccc.fare.core.dto.OrderCondition;
import com.gzccc.fare.core.model.Customer;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/30 16:29
 * @Version 1.0
 * @Content
 */
public interface CustomerService {
    List<AdminOrderCondition> indexQuery(AdminOrderCondition adminOrderCondition);

    List<CustomerCondition> query(CustomerCondition customerCondition);

    List<OrderCondition> list();

    List<CustomerCondition> customerList();

    void normal(Integer id);

    void forbidden(Integer id);
}
