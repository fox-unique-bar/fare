package com.gzccc.fare.admin.dao;

import com.gzccc.fare.core.dto.AdminOrderCondition;
import com.gzccc.fare.core.dto.CustomerCondition;
import com.gzccc.fare.core.dto.OrderCondition;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/30 16:33
 * @Version 1.0
 * @Content
 */
@Mapper
public interface CustomerDao {
    List<AdminOrderCondition> indexQuery(AdminOrderCondition adminOrderCondition);

    List<CustomerCondition> query(CustomerCondition customerCondition);

    List<OrderCondition> list();

    List<CustomerCondition> customerList();

    void normal(Integer id);

    void forbidden(Integer id);
}
