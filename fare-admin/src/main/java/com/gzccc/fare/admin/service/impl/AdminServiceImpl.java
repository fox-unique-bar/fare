package com.gzccc.fare.admin.service.impl;

import com.gzccc.fare.admin.dao.AdminDao;
import com.gzccc.fare.admin.service.AdminService;
import com.gzccc.fare.core.model.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/28 23:07
 * @Version 1.0
 * @Content
 */
@Service
public class AdminServiceImpl implements AdminService{
    @Autowired
    private AdminDao adminDao;
    @Override
    public String login(String useranme, String password) {
        Admin admin = adminDao.login(useranme);
        if (admin != null){
            if (admin.getPassword().equals(password)){
                return "登录成功";
            }
            return "密码错误";
        }
        return "账户不存在";
    }
}
