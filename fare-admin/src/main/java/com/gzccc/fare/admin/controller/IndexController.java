package com.gzccc.fare.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/29 2:0
 * @Version 1.0
 * @Content 管理员登录
 */
@Controller
public class IndexController {

    /**
     * 管理员主页面。重定向到主页面
     * @return
     */
    @GetMapping("/")
    public String index() {
        return "redirect:/admin/login";
    }
}
