package com.gzccc.fare.admin.service;

import com.gzccc.fare.core.dto.NoticeCondition;
import com.gzccc.fare.core.dto.TicketAddCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import com.gzccc.fare.core.model.Line;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/29 11:45
 * @Version 1.0
 * @Content
 */
public interface TicketService {
    List<TicketCondition> list();

    void add(TicketAddCondition ticketAddCondition);

    TicketCondition getId(Integer id);

    void edit(TicketCondition ticketCondition);


    List<TicketCondition> query(Line line);

    void delete(Integer id);

    void notice(NoticeCondition noticeCondition);

    List<NoticeCondition> noticeList();

}
