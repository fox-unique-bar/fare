package com.gzccc.fare.admin.dao;

import com.gzccc.fare.core.model.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/28 23:09
 * @Version 1.0
 * @Content
 */
@Mapper
public interface AdminDao {

    Admin login(String username);
}
