package com.gzccc.fare.admin.service.impl;

import com.gzccc.fare.admin.dao.TicketDao;
import com.gzccc.fare.admin.service.TicketService;
import com.gzccc.fare.core.dto.NoticeCondition;
import com.gzccc.fare.core.dto.TicketAddCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import com.gzccc.fare.core.model.Line;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Author: 杨友奇
 * @Date: 2018/10/29 11:56
 * @Version 1.0
 * @Content
 */
@Service
public class TicketServiceImpl implements TicketService{
    @Autowired
    private TicketDao ticketDao;
    @Override
    public List<TicketCondition> list() {
        return ticketDao.list();
    }

    @Override
    public void add(TicketAddCondition ticketAddCondition) {
        ticketDao.add(ticketAddCondition);
    }

    @Override
    public TicketCondition getId(Integer id) {
        return ticketDao.getId(id);
    }

    @Override
    public void edit(TicketCondition ticketCondition) {
         ticketDao.edit(ticketCondition);
    }

    @Override
    public List<TicketCondition> query(Line line) {
        return ticketDao.query(line);
    }

    @Override
    public void delete(Integer id) {
        ticketDao.delete(id);
    }

    @Override
    public void notice(NoticeCondition noticeCondition) {
        ticketDao.notice(noticeCondition);
    }

    @Override
    public List<NoticeCondition> noticeList() {
        return ticketDao.noticeList();
    }


}
