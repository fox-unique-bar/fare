package com.gzccc.fare.admin.service.impl;

import com.gzccc.fare.admin.dao.CustomerDao;
import com.gzccc.fare.admin.service.CustomerService;
import com.gzccc.fare.core.dto.AdminOrderCondition;
import com.gzccc.fare.core.dto.CustomerCondition;
import com.gzccc.fare.core.dto.OrderCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/30 16:32
 * @Version 1.0
 * @Content
 */
@Service
public class CustomerServiceImpl implements CustomerService{
    @Autowired
    private CustomerDao customerDao;
    @Override
    public List<AdminOrderCondition> indexQuery(AdminOrderCondition adminOrderCondition) {
        return customerDao.indexQuery(adminOrderCondition);
    }

    @Override
    public List<CustomerCondition> query(CustomerCondition customerCondition) {
        return customerDao.query(customerCondition);
    }

    @Override
    public List<OrderCondition> list() {
        return customerDao.list();
    }

    @Override
    public List<CustomerCondition> customerList() {
        return customerDao.customerList();
    }

    @Override
    public void normal(Integer id) {
        customerDao.normal(id);
    }

    @Override
    public void forbidden(Integer id) {
        customerDao.forbidden(id);
    }
}
