package com.gzccc.fare.admin.controller;

import com.gzccc.fare.admin.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/21 22:54
 * @Version 1.0
 * @Content 管理员操作
 */
@Controller
@RequestMapping("admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    /**
     * 进入到登录页面
     * @return
     */
    @GetMapping("/login")
    public String login(){
        return "/admin/login";
    }

    /**
     * 进行登录的账户密码判断
     * @param username
     * @param password
     * @param model
     * @param session
     * @return
     */
    @PostMapping("/login")
    public String login(@RequestParam String username, @RequestParam String password, Model model, HttpSession session){
        String login =  adminService.login(username, password);
        if (login.equals("登录成功")){
            //存储回话信息

            return "redirect:/ticket/index";

        }else {
            //提示错误信息
            model.addAttribute("errorMsg",login);
            return "admin/login";
        }
    }
}
