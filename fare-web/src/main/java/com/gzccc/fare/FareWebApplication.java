package com.gzccc.fare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 14:22
 * @Version 1.0
 * @Content
 */
@SpringBootApplication
public class FareWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(FareWebApplication.class,args);
    }
}
