package com.gzccc.fare.web.dao;

import com.gzccc.fare.core.dto.CustomerCondition;
import com.gzccc.fare.core.model.Admin;
import com.gzccc.fare.core.model.Customer;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:37
 * @Version 1.0
 * @Content
 */
@Mapper
public interface CustomerDao {
    CustomerCondition query(String useranme);

    void register(CustomerCondition customerCondition);

    void editCustomer(CustomerCondition customerCondition);
}
