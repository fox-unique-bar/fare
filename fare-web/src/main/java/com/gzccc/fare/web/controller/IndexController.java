package com.gzccc.fare.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:19
 * @Version 1.0
 * @Content 跳转到主页面
 */
@Controller
public class IndexController {
    @GetMapping("/")
    public String index() {
        return "redirect:/publiz/index";
    }
}
