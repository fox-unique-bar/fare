package com.gzccc.fare.web.controller;

import com.gzccc.fare.core.Util.SessionUtil;
import com.gzccc.fare.core.dto.NoticeCondition;
import com.gzccc.fare.core.dto.OrderCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import com.gzccc.fare.web.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:49
 * @Version 1.0
 * @Content
 */
@Controller
@RequestMapping("publiz")
public class PublizController {
    @Autowired
    private TicketService ticketService;

    @GetMapping("/index")
    public String index(Model model){
        List<NoticeCondition> noticeConditions = ticketService.noticeList();
        model.addAttribute("notice",noticeConditions);
        return "/publiz/index";
    }

    /**
     * 进入查询
     * @return
     */
    @GetMapping("/ticketQuery")
    public String ticketQuery(@Valid TicketCondition ticketCondition,Model model){
        //读取会话
        model.addAttribute("ticket",ticketCondition);
        //查询车讯信息
        List<TicketCondition> ticketConditions = ticketService.query(ticketCondition);
        //保存数据
        model.addAttribute("tickets",ticketConditions);
        return "publiz/ticketQuery";
    }
}
