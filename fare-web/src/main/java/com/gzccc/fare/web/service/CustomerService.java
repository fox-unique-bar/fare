package com.gzccc.fare.web.service;

import com.gzccc.fare.core.dto.CustomerCondition;

import javax.validation.Valid; /**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:38
 * @Version 1.0
 * @Content
 */
public interface CustomerService {
    String login(String username, String password);

    String register(CustomerCondition customerCondition);

    CustomerCondition query(String username);

    void editCustomer( CustomerCondition customerCondition);
}
