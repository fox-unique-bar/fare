package com.gzccc.fare.web.controller;

import com.gzccc.fare.core.Util.SessionUtil;
import com.gzccc.fare.core.dto.OrderCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import com.gzccc.fare.core.model.Line;
import com.gzccc.fare.web.dao.OrderDao;
import com.gzccc.fare.web.service.OrderService;
import com.gzccc.fare.web.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 16:28
 * @Version 1.0
 * @Content
 */
@Controller
@RequestMapping("ticket")
public class TicketController {
    @Autowired
    private TicketService ticketService;
    @Autowired
    private OrderService orderService;

    /**
     * 进入查询车讯主页面
     * @return
     */
    @GetMapping("/query")
    public String query(@Valid TicketCondition ticketCondition, HttpSession session,Model model){
        //读取会话信息
        SessionUtil.getCustomerSession(session,model,"customer");
        //读取会话,目的是保存搜索框上面的起点跟终点站
        model.addAttribute("ticket",ticketCondition);
        //查询车讯信息
        List<TicketCondition> ticketConditions = ticketService.query(ticketCondition);
        //保存数据
        model.addAttribute("tickets",ticketConditions);
        return "ticket/query";
    }

    /**
     * 进入车票预购页面
     * @param id
     * @param session
     * @param model
     * @return
     */
    @GetMapping("/place")
    public String place(@Valid Integer id, HttpSession session,Model model){
        //读取会话信息
        SessionUtil.getCustomerSession(session,model,"customer");
        //通过id查询车讯信息
        TicketCondition ticketConditions = ticketService.queryId(id);
        //保存数据
        model.addAttribute("tickets",ticketConditions);
        return "ticket/place";
    }

    /**
     * 点击下单触发，后台修改车位表，并产生一张新的订单表
     * @param orderCondition
     * @param session
     * @param model
     * @return
     */
    @PostMapping("/place")
    public String place(@Valid OrderCondition orderCondition, HttpSession session, Model model){
        //读取会话信息
        SessionUtil.getCustomerSession(session,model,"customer");
        //修改车位信息
        ticketService.editTicket(orderCondition);
        //产生一张新的订单表
        orderService.newOrder(orderCondition);
        return "/ticket/success_place";
    }

    @GetMapping("/success_place")
    public String success_place(HttpSession session,Model model){
        //读取会话信息
        SessionUtil.getCustomerSession(session,model,"customer");
        return "ticket/success_place";
    }

}
