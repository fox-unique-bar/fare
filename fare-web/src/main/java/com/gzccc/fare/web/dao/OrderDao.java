package com.gzccc.fare.web.dao;

import com.gzccc.fare.core.dto.OrderCondition;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:37
 * @Version 1.0
 * @Content
 */
@Mapper
public interface OrderDao {
    void newOrder(OrderCondition orderCondition);

    List<OrderCondition> list(String id);

    OrderCondition query(String id);

    void editOrder(String pleat_number);

    void deleteOrder(int id);
}
