package com.gzccc.fare.web.service;

import com.gzccc.fare.core.dto.NoticeCondition;
import com.gzccc.fare.core.dto.OrderCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import com.gzccc.fare.core.model.Line;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/6 14:25
 * @Version 1.0
 * @Content
 */
public interface TicketService {
    List<NoticeCondition> noticeList();

    List<TicketCondition> query(TicketCondition ticketCondition);

    TicketCondition queryId(Integer id);


    void editTicket(OrderCondition orderCondition);
}
