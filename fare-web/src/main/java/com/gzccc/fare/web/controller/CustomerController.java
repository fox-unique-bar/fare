package com.gzccc.fare.web.controller;

import com.gzccc.fare.core.Util.ResultUtil;
import com.gzccc.fare.core.Util.SessionUtil;
import com.gzccc.fare.core.dto.CustomerCondition;
import com.gzccc.fare.core.dto.NoticeCondition;
import com.gzccc.fare.web.service.CustomerService;
import com.gzccc.fare.web.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:36
 * @Version 1.0
 * @Content 客户管理
 */
@Controller
@RequestMapping("customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private TicketService ticketService;
    /**
     * 进入登录页面
     * @return
     */
    @GetMapping("/login")
    public String login(){
        return "/customer/login";
    }

    /**
     * 进行登录的账户密码判断
     * @param username
     * @param password
     * @param model
     * @param session
     * @return
     */
    @PostMapping("/login")
    public String login(@RequestParam String username, @RequestParam String password, Model model, HttpSession session){
        String login =  customerService.login(username, password);
        if (login.equals("登录成功")){
            //存储会话信息
            CustomerCondition customerCondition = customerService.query(username);
            session.setAttribute("customer", customerCondition);
            return "redirect:/customer/index";
        }else {
            model.addAttribute("errorMsg",login);
            return "customer/login";
        }
    }

    /**
     * 进入注册页面
     * @return
     */
    @GetMapping("/register")
    public String register(){
        return "/customer/register";
    }

    @PostMapping("/register")
    public String register(@Valid CustomerCondition customerCondition,Model model){
            String register = customerService.register(customerCondition);
            if (register.equals("注册成功")){
                return "redirect:/customer/login";
            }else {
                model.addAttribute("errorMsg",register);
                return "customer/register";
            }

    }

    /**
     * 进入主页面
     * @return
     */
    @GetMapping("/index")
    public String index(HttpSession session,Model model){
        //获取回话信息
        SessionUtil.getCustomerSession(session,model,"customer");

        List<NoticeCondition> noticeConditions = ticketService.noticeList();
        if (noticeConditions.size() != 0){
            model.addAttribute("notice",noticeConditions);
        }else {
            model.addAttribute("error","无公告");
        }
        return "/customer/index";
    }



    /**
     * 进入用户个人信息
     * @return
     */
    @GetMapping("/query")
    public String query(HttpSession session,Model model){
        SessionUtil.getCustomerSession(session,model,"customer");
        return "/customer/query";
    }

    /**
     * 进入用户个人信息修改页面
     * @return
     */
    @GetMapping("/edit")
    public String edit(@Valid CustomerCondition customerCondition,HttpSession session,Model model){
        SessionUtil.getCustomerSession(session,model,"customer");
        return "/customer/edit";
    }
    /**
     * 进入用户个人信息修改页面
     * @return
     */
    @PostMapping("/edit")
    public String edit(@Valid CustomerCondition customerCondition,Model model,HttpSession session){
        SessionUtil.getCustomerSession(session,model,"customer");
        customerService.editCustomer(customerCondition);
        return "/customer/success_edit";
    }

    /**
     * 进入用户修改个人信息成功页面
     * @return
     */
    @GetMapping("/success_edit")
    public String success_edit(HttpSession session,Model model){
        //读取会话信息
        SessionUtil.getCustomerSession(session,model,"customer");
        return "customer/success_edit";
    }
}
