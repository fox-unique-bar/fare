package com.gzccc.fare.web.controller;

import com.gzccc.fare.core.Util.SessionUtil;
import com.gzccc.fare.core.dto.OrderCondition;
import com.gzccc.fare.web.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:36
 * @Version 1.0
 * @Content 订单管理
 */
@Controller
@RequestMapping("order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * 进入用户我的订单页面
     * @return
     */
    @GetMapping("/index")
    public String index(@RequestParam String id, HttpSession session, Model model){
        SessionUtil.getCustomerSession(session,model,"customer");
        List<OrderCondition> orderConditions = orderService.list(id);
        model.addAttribute("orders",orderConditions);
        return "/order/index";
    }
    @PostMapping("/index")
    public String index(@RequestParam String id,Model model,HttpSession session){
        SessionUtil.getCustomerSession(session,model,"customer");
        //通过ID查询该订单信息，并保存到orderCondition中
        OrderCondition condition = orderService.query(id);
        //通过orderCondition中的pleat_number查询到coach表中进行修改，修改成功便删除该订单
        orderService.editOrder(condition);
        return "redirect:/order/index?id="+condition.getCustomer_id();
    }
    /**
     * 进入用户订单改签页面
     * @return
     */
    @GetMapping("/edit")
    public String edit(){
        return "/order/edit";
    }

}
