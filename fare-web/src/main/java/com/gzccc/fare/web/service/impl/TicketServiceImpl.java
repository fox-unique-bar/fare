package com.gzccc.fare.web.service.impl;

import com.gzccc.fare.core.dto.NoticeCondition;
import com.gzccc.fare.core.dto.OrderCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import com.gzccc.fare.core.model.Line;
import com.gzccc.fare.web.dao.TicketDao;
import com.gzccc.fare.web.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/6 14:26
 * @Version 1.0
 * @Content
 */
@Service
public class TicketServiceImpl implements TicketService{
   @Autowired
   private TicketDao ticketDao;

    @Override
    public List<NoticeCondition> noticeList() {
        return ticketDao.noticeList();
    }

    @Override
    public List<TicketCondition> query(TicketCondition ticketCondition) {
        return ticketDao.query(ticketCondition);
    }

    @Override
    public TicketCondition queryId(Integer id) {
        return ticketDao.queryId(id);
    }

    @Override
    public void editTicket(OrderCondition orderCondition) {
        ticketDao.editTicket(orderCondition);
    }


}
