package com.gzccc.fare.web.service.impl;

import com.gzccc.fare.core.dto.CustomerCondition;
import com.gzccc.fare.core.model.Admin;
import com.gzccc.fare.core.model.Customer;
import com.gzccc.fare.web.dao.CustomerDao;
import com.gzccc.fare.web.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:39
 * @Version 1.0
 * @Content
 */
@Service
public class CustomerServiceImpl implements CustomerService{
    @Autowired
    private CustomerDao customerDao;
    @Override
    public CustomerCondition query(String username) {
        return customerDao.query(username);
    }

    @Override
    public void editCustomer(CustomerCondition customerCondition) {
        customerDao.editCustomer(customerCondition);
    }

    @Override
    public String login(String useranme, String password) {
        CustomerCondition customer = customerDao.query(useranme);
        if (customer != null){
            if (customer.getPassword().equals(password)){
                //判断用户是否被禁用
                if (customer.getStatus() == 1){
                    return "登录成功";
                }else {
                    return "您的账号被禁用";
                }
            }
            return "密码错误";

        }
        return "账户不存在";
    }

    @Override
    public String register(CustomerCondition customerCondition) {
        CustomerCondition customer = customerDao.query(customerCondition.getUsername());
        if (customer == null){
            customerDao.register(customerCondition);
            return "注册成功";
        }else {
            return "账号已注册";
        }
    }



}
