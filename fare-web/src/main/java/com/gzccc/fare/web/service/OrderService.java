package com.gzccc.fare.web.service;

import com.gzccc.fare.core.dto.OrderCondition;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:38
 * @Version 1.0
 * @Content
 */
public interface OrderService {
    void newOrder(OrderCondition orderCondition);

    List<OrderCondition> list(String id);


    OrderCondition query(String id);

    void editOrder(OrderCondition condition);
}
