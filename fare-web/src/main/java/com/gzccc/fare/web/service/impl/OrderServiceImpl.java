package com.gzccc.fare.web.service.impl;

import com.gzccc.fare.core.dto.OrderCondition;
import com.gzccc.fare.web.dao.OrderDao;
import com.gzccc.fare.web.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.beans.Transient;
import java.util.List;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/2 15:39
 * @Version 1.0
 * @Content
 */
@Service
public class OrderServiceImpl implements OrderService{
    @Autowired
    private OrderDao orderDao;

    @Override
    public void newOrder(OrderCondition orderCondition) {
        orderDao.newOrder(orderCondition);
    }

    @Override
    public List<OrderCondition> list(String id) {
        return orderDao.list(id);

    }

    @Override
    public OrderCondition query(String id) {
        return orderDao.query(id);
    }

    @Override
    @Transactional
    public void editOrder(OrderCondition condition) {
        orderDao.editOrder(condition.getPleat_number());
        orderDao.deleteOrder(condition.getId());
    }


}
