window.onload = function(){
	var banner = document.getElementById('banner');
	var list = document.getElementById('lists');
	var img_btn = document.getElementById('imgbtns').getElementsByTagName('span');
	var prev = document.getElementById('prev');
	var next = document.getElementById('next');
	var index = 1;
	var animated = false;
	var timer;

	function animate(offset){
		animated = true;
		var time = 500;//位移时间
		var evertime = 10;//位移间隔时间
		var speed = offset/(time/evertime);
		var newleft = parseInt(list.style.left) + offset;

		function go(){
			if((speed<0&&parseInt(list.style.left)>newleft)||(speed>0&&parseInt(list.style.left)<newleft)){
				list.style.left = parseInt(list.style.left) + speed + 'px';
				setTimeout(go,evertime);
			}else{
				animated = false;
				list.style.left = newleft + "px";
				if(newleft > -1170){
					list.style.left = -5850 + "px";
				}
				if(newleft < -5850){
					list.style.left = -1170 + "px";
				}
			}
		}
		go();
	}

	function showbtn(){
		for(var i=0;i<img_btn.length;i++){
			if(img_btn[i].className == 'on'){
				img_btn[i].className = '';
				break;
			}
		}
		img_btn[index - 1].className = 'on';
	}
	//点击按钮切换
	for (var i = 0; i < img_btn.length; i++) {
		img_btn[i].onclick = function(){
			if(this.className == 'on'){
				return;
			}
			var myindex = parseInt(this.getAttribute('index'));
			var offset = -1170 * (myindex - index);
			if(animated == false){
				animate(offset);
		    }
			index = myindex;
			showbtn();
 		}
	}

	prev.onclick = function(){
		if(animated == false){
			animate(1170);
		}
		
		if(index == 1){
			index = 5;
		}else{
		    index -= 1;
	    }
		showbtn();
	}
	next.onclick = function(){
		if(animated == false){
			animate(-1170);
		}
		if(index == 5){
			index = 1;
		}else{
			index += 1;
		}
		showbtn();
	}
	function play(){
		timer = setInterval(function(){
			next.onclick();
		},5000);
	}
	function stop(){
		clearInterval(timer);
	}
	banner.onmouseover = function(){
		stop();
		prev.style.display = 'block';
		next.style.display = 'block';
	}
	banner.onmouseout = function(){
		play();
		prev.style.display = 'none';
		next.style.display = 'none';
	} 
	play();
}