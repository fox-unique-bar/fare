DROP DATABASE automobile;
CREATE DATABASE automobile;

/* 创建管理员表 */
CREATE TABLE admin(
  admin_id int PRIMARY KEY AUTO_INCREMENT COMMENT '管理员id',
  username VARCHAR(5) NOT NULL COMMENT '姓名',
  password VARCHAR(20) NOT NULL COMMENT '用户密码',
  status VARCHAR(20) NOT NULL DEFAULT 'admin' COMMENT '身份'
);

/* 创建车票表 */
CREATE TABLE ticket(
  id int PRIMARY KEY AUTO_INCREMENT COMMENT 'id',
  ticket_price SMALLINT NOT NULL COMMENT '车票价格',
  departure_time DATETIME NOT NULL COMMENT '发车时间',
  pleat_number VARCHAR(20)  NOT NULL COMMENT '汽车信息编号',
  line_number VARCHAR(20)  NOT NULL COMMENT '线路信息编号'
);
/* 创建汽车信息表*/
CREATE TABLE coach(
  id int PRIMARY KEY AUTO_INCREMENT COMMENT '汽车信息id',
  seating SMALLINT DEFAULT '50' NOT NULL COMMENT '座位数',
  pleat_number VARCHAR(20) NOT NULL COMMENT '车牌号'
);
/* 创建线路表 */
CREATE TABLE line(
  line_id int PRIMARY KEY AUTO_INCREMENT COMMENT '线路id',
  line_number VARCHAR(20) NOT NULL COMMENT '线路编号',
  starting_station VARCHAR(20) NOT NULL COMMENT '起始站',
  destination VARCHAR(20)  NOT NULL COMMENT '终点站'
);

/* 创建客户表*/
CREATE TABLE Customer(
  Customer_id int PRIMARY KEY AUTO_INCREMENT COMMENT '客户id',
  username VARCHAR(20) NOT NULL COMMENT '账户名',
  password VARCHAR(20) NOT NULL COMMENT '用户密码',
  nick_name VARCHAR(5) NOT NULL COMMENT '昵称',
  number VARCHAR(11) NOT NULL COMMENT'手机号码',
  status SMALLINT NOT NULL DEFAULT '1' COMMENT '权限'
);

/*创建订单表*/
CREATE TABLE Orders(
  id int PRIMARY KEY AUTO_INCREMENT COMMENT 'id',
  ticket_price SMALLINT NOT NULL COMMENT '车票价格',
  seat_number SMALLINT NOT NULL COMMENT '车位号',
  departure_time DATETIME NOT NULL COMMENT '发车时间',
  coach_number VARCHAR(20)  NOT NULL COMMENT '汽车信息编号',
  line_number VARCHAR(20)  NOT NULL COMMENT '线路信息编号',
  Customer_id int NULL COMMENT '客户信息编号'
);
/*创建公告*/
CREATE TABLE notice(
  id int PRIMARY KEY AUTO_INCREMENT COMMENT 'id',
  title VARCHAR(20) NOT NULL COMMENT '公告标题',
  content VARCHAR(255) NOT NULL COMMENT '内容'
);



INSERT INTO admin(username,password,status) VALUES ('admin','123456','admin'),('root','root','admin');
INSERT INTO Customer(username,password,nick_name,number,status) VALUES('15915514516','123456','杨友奇','15915514516',1),('li','123','李振鹏','15915514517',1),('chen','123','陈鑫豪','15915514517',1);
INSERT INTO coach(seating,pleat_number)VALUES('50','粤A 88888'),('50','粤B 88888'),('50','粤C 88888'),('50','粤D 88888'),('50','粤E 88888');
INSERT INTO line(line_number,starting_station,destination)VALUES('FZ128','广州','深圳'),('FZ129','广州','杭州');
INSERT INTO ticket(ticket_price,departure_time,pleat_number,line_number)VALUES('80','2018-10-18 20:00:00','粤A 88888','FZ128'),('80','2018-10-18 20:00:00','粤B 88888','FZ128');