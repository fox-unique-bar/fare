package com.gzccc.fare.core.dto;

import java.util.Date;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/30 16:22
 * @Version 1.0
 * @Content
 */
public class OrderCondition {
    /* 订单编号 */
    private int id;
    /* 发车时间 */
    private String departure_time;
    /* 车票价格 */
    private int ticket_price;
    /* 座位号 */
    private int seat_number;
    /* 车牌号 */
    private String pleat_number;
    /* 起始站 */
    private  String starting_station;
    /* 终点站 */
    private  String destination;
    /* 用户名*/
    private Integer customer_id;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public int getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(int ticket_price) {
        this.ticket_price = ticket_price;
    }

    public int getSeat_number() {
        return seat_number;
    }

    public void setSeat_number(int seat_number) {
        this.seat_number = seat_number;
    }

    public String getStarting_station() {
        return starting_station;
    }

    public void setStarting_station(String starting_station) {
        this.starting_station = starting_station;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getPleat_number() {
        return pleat_number;
    }

    public void setPleat_number(String pleat_number) {
        this.pleat_number = pleat_number;
    }
}
