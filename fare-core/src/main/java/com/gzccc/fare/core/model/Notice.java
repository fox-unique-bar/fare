package com.gzccc.fare.core.model;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/30 17:36
 * @Version 1.0
 * @Content
 */
public class Notice {
    /*公告ID*/
    private int id;
    /*标题*/
    private String title;
    /*内容*/
    private String content;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
