package com.gzccc.fare.core.Util;


import com.gzccc.fare.core.dto.CustomerCondition;
import com.gzccc.fare.core.dto.TicketCondition;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

/**
 * @Author: 杨友奇
 * @Date: 2018/11/6 14:51
 * @Version 1.0
 * @Content 返回用户信息
 */
public class SessionUtil {

    public static  void getCustomerSession(HttpSession session, Model model, String key){
         CustomerCondition customerCondition = (CustomerCondition) session.getAttribute("customer");
        model.addAttribute(key, customerCondition);
    }
    public static  void getTicketSession(HttpSession session, Model model, String key){
        TicketCondition ticketCondition = (TicketCondition) session.getAttribute("ticket");
        model.addAttribute(key, ticketCondition);
    }
}
