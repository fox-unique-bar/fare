package com.gzccc.fare.core.model;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/18 10:56
 * @Version 1.0
 * @Content 创建管理员账户
 */
public class Admin {
    private int id;
    /* 管理员账户 */
    private String username;
    /* 管理员密码 */
    private String password;
    /* 管理员身份，默认是admin */
    private int status;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

