package com.gzccc.fare.core.dto;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/22 13:13
 * @Version 1.0
 * @Content 车讯添加
 */
public class TicketAddCondition {
    /* 车票价格 */
    private int ticket_price;
    /* 发车时间 */
    private String departure_time;
    /* 剩余座位数 */
    private int seating;
    /* 车牌号 */
    private String pleat_number;
    /* 线路编号*/
    private String line_number;
    /* 起始站 */
    private  String starting_station;
    /* 终点站 */
    private  String destination;

    public int getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(int ticket_price) {
        this.ticket_price = ticket_price;
    }
    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public int getSeating() {
        return seating;
    }

    public void setSeating(int seating) {
        this.seating = seating;
    }

    public String getPleat_number() {
        return pleat_number;
    }

    public void setPleat_number(String pleat_number) {
        this.pleat_number = pleat_number;
    }

    public String getLine_number() {
        return line_number;
    }

    public void setLine_number(String line_number) {
        this.line_number = line_number;
    }

    public String getStarting_station() {
        return starting_station;
    }

    public void setStarting_station(String starting_station) {
        this.starting_station = starting_station;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
