package com.gzccc.fare.core.model;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/18 11:07
 * @Version 1.0
 * @Content 创建汽车信息表
 */
public class Coach {
    private int id;
    /*汽车编号*/
    private String coach_number;
    /* 座位数 */
    private int seating;
    /* 剩余座位数 */
    private int parking_space;
    /* 车牌号 */
    private String pleat_number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getSeating() {
        return seating;
    }

    public void setSeating(int seating) {
        this.seating = seating;
    }

    public int getParking_space() {
        return parking_space;
    }

    public void setParking_space(int parking_space) {
        this.parking_space = parking_space;
    }

    public String getPleat_number() {
        return pleat_number;
    }

    public void setPleat_number(String pleat_number) {
        this.pleat_number = pleat_number;
    }

    public String getCoach_number() {
        return coach_number;
    }

    public void setCoach_number(String coach_number) {
        this.coach_number = coach_number;
    }
}
