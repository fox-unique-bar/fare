package com.gzccc.fare.core.dto;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/30 16:22
 * @Version 1.0
 * @Content
 */
public class AdminOrderCondition {
    /* 订单编号 */
    private int id;
    /* 发车时间 */
    private String departure_time;
    /* 车票价格 */
    private int ticket_price;
    /* 座位号 */
    private int seat_number;
    /* 起始站 */
    private  String starting_station;
    /* 终点站 */
    private  String destination;
    /* 用户名*/
    private String username;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public int getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(int ticket_price) {
        this.ticket_price = ticket_price;
    }

    public int getSeat_number() {
        return seat_number;
    }

    public void setSeat_number(int seat_number) {
        this.seat_number = seat_number;
    }

    public String getStarting_station() {
        return starting_station;
    }

    public void setStarting_station(String starting_station) {
        this.starting_station = starting_station;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
