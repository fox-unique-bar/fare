package com.gzccc.fare.core.Util;


import com.gzccc.fare.core.model.Msg;

/**
 * 统一的返回信息格式
 */
public class ResultUtil {
    /**
     * 请求成功返回
     * @param object
     * @return
     */
    public static Msg success(Object object){
        Msg msg = new Msg();
        msg.setCode(200);
        msg.setMsg("请求成功");
        msg.setData(object);
        return msg;
    }
    public static Msg success(){
        return success(null);
    }

    /**
     * 请求失败返回
     * @param code
     * @param resultmsg
     * @return
     */
    public static Msg error(Integer code, String resultmsg){
        Msg msg = new Msg();
        msg.setCode(code);
        msg.setMsg("内部错误");
        msg.setData(resultmsg);
        return msg;
    }
}
