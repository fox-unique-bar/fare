package com.gzccc.fare.core.model;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/18 11:12
 * @Version 1.0
 * @Content 创建线路表
 */
public class Line {
    private int id;
    /* 线路编号*/
    private String line_number;
    /* 起始站 */
    private  String starting_station;
    /* 终点站 */
    private  String destination;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLine_number() {
        return line_number;
    }

    public void setLine_number(String line_number) {
        this.line_number = line_number;
    }

    public String getStarting_station() {
        return starting_station;
    }

    public void setStarting_station(String starting_station) {
        this.starting_station = starting_station;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "Line{" +
                "id=" + id +
                ", line_number='" + line_number + '\'' +
                ", starting_station='" + starting_station + '\'' +
                ", destination='" + destination + '\'' +
                '}';
    }
}
