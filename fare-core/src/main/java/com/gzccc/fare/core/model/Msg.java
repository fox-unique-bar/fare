package com.gzccc.fare.core.model;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/18 11:00
 * @Version 1.0
 * @Content 统一返回格式
 */
public class Msg<T> {
    /*错误码*/
    private Integer code;

    /*提示信息 */
    private String msg;

    /*具体内容*/
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

