package com.gzccc.fare.core.dto;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/30 17:02
 * @Version 1.0
 * @Content
 */
public class CustomerCondition {
    private  int id;
    /* '账户名'*/
    private String username;
    /*'用户密码' */
    private String password;
    /* 昵称*/
    private String nick_name;
    /* 手机号码*/
    private String number;
    /* 权限*/
    private int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
