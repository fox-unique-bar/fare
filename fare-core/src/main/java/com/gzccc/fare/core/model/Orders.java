package com.gzccc.fare.core.model;

import java.util.Date;

/**
 * @Author: 杨友奇
 * @Date: 2018/10/22 16:15
 * @Version 1.0
 * @Content
 */
public class Orders {
    private int id;
    /* 车票编号 */
    private String ticket_id;
    /* 车票价格 */
    private int ticket_price;
    /* 座位号 */
    private int seat_number;
    /* 发车时间 */
    private Date departure_time;

    /* 汽车信息表 关系为多对一*/
    private Coach coach;

    /* 线路信息表 关系为多对一*/
    private Line line;

    /* 客户关系表 关系为多对一*/
    private Customer customer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public int getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(int ticket_price) {
        this.ticket_price = ticket_price;
    }

    public int getSeat_number() {
        return seat_number;
    }

    public void setSeat_number(int seat_number) {
        this.seat_number = seat_number;
    }

    public Date getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(Date departure_time) {
        this.departure_time = departure_time;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
